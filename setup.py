from common.version_manager import get_version_info
from setuptools import setup, find_packages


setup(name='abm-builder',
      version=get_version_info(),
      description='Custom libraries for building. An alternative to Make, CMake and similar. In Python, just because, why not?',
      author='Vas',
      author_email='guttina.srinivas.at.THE_FAMOUS_MAIL_PROVIDER',
      license='All rights reserved.',
      packages=find_packages(exclude=['']),
      install_requires=['easygui']
      )


# End of file

