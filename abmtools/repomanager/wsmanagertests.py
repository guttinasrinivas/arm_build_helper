import sys
import logging
from utils.logger_setup import setup_logging
from repomanager.WorkSpaceManager import WorkSpace


repos = dict()
repos['FW_03rdJune2016'] = 'FW_03rdJune2016'
repos['FW_18thAug2017'] = 'FW_18thAug2017'


def ws_setup():
    sandbox_loc = '/home/gsjy/temp/src/test'
    repo_remote = '/home/gsjy/src/work/Stanley'
    wspace = WorkSpace(sandbox_loc, repo_remote)
    wspace.setup(repos)
    wspace.run(sys.argv[1])


def main():
    setup_logging(chlevel=logging.DEBUG, fhlevel=logging.DEBUG)
    log = logging.getLogger(__name__)
    log.info('Starting up...')
    ws_setup()


if __name__ == "__main__":
    main()
