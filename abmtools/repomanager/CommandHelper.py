import os
import logging
import subprocess


class GitHelper:
    def __init__(self, gitpath='', remotepath='', repodir=os.getcwd()):
        self.repodir = repodir
        self.gitpath = gitpath
        while remotepath[-1] == '/':
            remotepath = remotepath[:-1]
        self.remotepath = remotepath
        self.reponame = remotepath.split('/')[-1]
        self.logger = logging.getLogger(__name__)

    def add(self, filename, opts=[]):
        cmdopts = [filename]
        cmdopts.extend(opts)
        return self.run_cmd('add', cmdopts)

    def clone(self, remote=None, opts=[]):
        if remote is None:
            remote = self.remotepath
        cmdopts = [remote, self.repodir]
        cmdopts.extend(opts)
        return self.run_cmd('clone', cmdopts, os.getcwd())

    def update(self, opts=[]):
        return self.run_cmd('update', opts)
    
    def commit(self, opts=[]):
        cwd = self.get_cwd()
        return self.run_cmd('commit', opts, cwd=cwd)
    
    def pull(self, opts=[]): 
        return self.run_cmd('pull', opts)
    
    def push(self, opts=[]): 
        return self.run_cmd('push', opts)
    
    def status(self, opts=[]):
        return self.run_cmd('status', opts)

    def stash_save(self, opts=[]):
        cmdtops = ['save']
        cmdopts.extend(opts)
        return self.run_cmd('stash', cmdopts)

    def stash_pop(self, opts=[]):
        cmdtops = ['pop']
        cmdopts.extend(opts)
        return self.run_cmd('stash', cmdopts)

    def get_cwd(self):
        cwd = self.repodir + os.path.sep + self.reponame
        if not os.path.exists(cwd):
            cwd = self.repodir
        if not os.path.exists(cwd):
            raise RuntimeError('Repo does not exist: {}'.format(cwd))
        return cwd

    def run_cmd(self, req, opts=[], cwd=None, simulate=False):
        log = logging.getLogger(__name__)
        cmd = [self.gitpath + os.path.sep + 'git']
        cmd.append(req)
        cmd.extend(opts)
        if cwd is None:
            cwd = self.repodir
        log.info('Running command: {} in {}'.format(' '.join(cmd), cwd))
        if not simulate:
            p = subprocess.Popen(cmd, cwd=cwd)
            p.wait()

