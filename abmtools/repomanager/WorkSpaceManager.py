import os
import sys
import logging
import subprocess
from repomanager.CommandHelper import GitHelper
from repomanager.WorkSpaceBuilder import build_workspace


class WorkSpace:
    def __init__(self, wspath=os.getcwd(), remotepath='', key=None, user=None):
        log = logging.getLogger(__name__)
        log.info("Initializing workspace at {} from repos at {}...".format(wspath, remotepath))
        self.wspath = wspath
        self.remotepath = remotepath
        self.pvtkey = key
        self.user = user
        self.repos = dict()
        self.cmds = {'clone': self.clone,
                     'pull': self.pull,
                     'push': self.push,
                     'commit': self.commit,
                     'sync': self.sync,
                     'list': self.list,
                     'status': self.status,
                     'build': self.build}

    def setup(self, repos):
        log = logging.getLogger(__name__)
        log.info("Setting up for {}".format(repos))
        for reponame in repos.keys():
            path = repos[reponame]
            repopath = self.wspath + os.path.sep + reponame
            remote = ''
            if self.user is not None:
                remote += self.user + '@'
            remote += self.remotepath + os.path.sep + path
            log.info("Setting up {} at {}...".format(remote, repopath))
            repo = GitHelper('/usr/bin', remote, repopath)
            self.repos[reponame] = repo

    def run(self, *args, **kwargs):
        """ Usage: WorkSpace.run(cmd, *cmdargs)"""
        log = logging.getLogger(__name__)
        cmd = args[0]
        if cmd not in self.cmds.keys():
            log.error("Unknown command: {}".format(cmd))
            self.print_help()
            return
        handler = self.cmds[cmd]
        return handler()

    def print_help(self):
        log = logging.getLogger(__name__)
        log.info("{} <cmd> <opts>".format(sys.argv[0]))
        log.info("  Available commands are {}".format(', '.join(self.cmds.keys())))

    def check_repopath(self, repopath):
        log = logging.getLogger(__name__)
        dirpath = repopath
        log.info("Checking for {} path...".format(dirpath))
        if os.path.exists(dirpath):
            raise RuntimeError("Path already exists: {}".format(repopath))
        dirchain = dirpath.split(os.path.sep)
        currpath = os.path.sep.join(dirchain[:-2])
        if not os.path.exists(currpath):
            raise RuntimeError("Invalid repo path: {}".format(repopath))
        currpath = os.path.sep.join(dirchain[:-1])
        if not os.path.exists(currpath):
            os.mkdir(currpath)

    def clone(self):
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            log.info("Cloning {} from {}...".format(reponame, repo.remotepath))
            self.check_repopath(repo.repodir)
            repo.clone()

    def push(self):
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            log.info("Attempting to push changes from {} to {}...".format(reponame, repo.remotepath))
            repo.push(opts=['--tags'])

    def pull(self):
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            log.info("Attempting to pull changes from {}...".format(repo.gitpath))
            repo.pull()

    def status(self):
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            repo.status()

    def stash_save(self):
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            log.info("Stashing changes in {}...".format(repo.gitpath))
            repo.stash_save()

    def stash_pop(self):
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            log.info("Attempting restore from stash in {}...".format(repo.gitpath))
            repo.stash_pop()

    def commit(self): 
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            log.info("Attempting to commit to {}...".format(repo.repodir))
            repo.commit(['--all'])

    def sync(self):
        log = logging.getLogger(__name__)
        log.info("Attemtping to synchronize WorkSpace...")
        self.stash_save()
        self.pull()
        self.stash_pop()
        self.commit()
        self.push()

    def list(self):
        log = logging.getLogger(__name__)
        retbuf = []
        for reponame in self.repos.keys():
            repo = self.repos[reponame]
            retbuf.append('{}: {}'.format(reponame, repo.repodir))
        return '\n'.join(retbuf)
    
    def build(self):
        log = logging.getLogger(__name__)
        for reponame in self.repos.keys():
            exe_name = reponame + '.exe'
            repo = self.repos[reponame]
            log.info("Attempting to build {}...".format(repo.repodir))
            os.chdir(repo.repodir)
            build_workspace(exe_name)


