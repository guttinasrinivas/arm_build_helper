#!/usr/bin/python3
import os
import sys
import json
import subprocess
from builder.helper.armtools.helper import Helper as BuildHelper


def conf_flags(builder):
    builder.LDFLAGS.append("-Wl,-Map=build/{0:}.map".format(builder.ONAME))
    builder.INCDIRS = [ "$HOME/src/embedded/common/inc",
            ".",
            "stm_lib",
            "stm_lib/inc",
            "stm_lib/src",
            "cmsis_boot",
            "cmsis_boot/startup",
            "cmsis"
            ]

    builder.CFLAGS = [
            "-c",
            "-Os",
            "-std=c99",
            "-g",
            "-mthumb",
            "-mcpu=cortex-m3",
            "-msoft-float",
            "-mfloat-abi=soft",
            "-mfix-cortex-m3-ldrd",
            "-Wextra",
            "-Wshadow",
            "-Wimplicit-function-declaration",
            "-Wredundant-decls",
            "-Wmissing-prototypes",
            "-Wstrict-prototypes",
            "-fno-common",
            "-ffunction-sections",
            "-fdata-sections",
            "-fpack-struct",
            "-MD",
            "-Wall",
            "-Wno-sign-compare",
            "-Wundef",
            "-DSTM32F103C8Tx",
            "-DSTM32",
            "-DSTM32F1",
            "-DSTM32F10X_MD",
            "-D__CM3_REV=0"]

    # Order is very important in libraries
    # builder.LIBS = []

#--------====----====----====----====----====----====--------
# Standard - Boiler Plate stuff. Our builder could build the
# code, but We still need someone run our builder. Oh, the
# irony...
#--------====----====----====----====----====----====--------

#------------------------------------------------------------
# Area 51
# =======
# No need to modify anything here, unless it is working.
#
# It is not expected to work. If it works, something is
# seriously wrong with the space-time fabric. Let me know
# righaway and I'll try my best to fix it.
#------------------------------------------------------------
def build_workspace(exe_name, skip_clean=True):
    builder = BuildHelper()
    builder.DEBUG = False 
    builder.ONAME = exe_name
    conf_flags(builder)
    builder.skip_clean = skip_clean
# Start with the clean()
    builder.clean_prev_build()
# And build...
    builder.set_sources()
    builder.compile()
    builder.link()
    builder.objcopy()
