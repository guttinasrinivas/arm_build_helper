import os
import sys
import json
import subprocess
import logging
from abmtools.utils.logger_setup import setup_logging


class Helper:
    def __init__(self):
        # Setup a build logger if the logging is not setup yet
        if len(logging.getLogger().handlers) == 0:
            setup_logging(logfname="build.log")
        self.DEBUG = False
        self.CC_PREFIX = 'arm-none-eabi-'
        self.CC = 'gcc'
        self.logfd = open('__build_cmd.log', 'w+')
        self.last_err = 0
        self.skip_clean = False
        self.CFLAGS = []
        self.LDFLAGS = []
        self.SOURCES = []
        self.INCDIRS = []
        self.LIBS = []
        self.LIBPATHS = []
        self.obj_files = []
        self.ONAME = 'a.out'
        self.LDSCRIPT = 'LinkerScript.ld'
        self.cmdlog = open('build_cmds.log', 'w+')
        self.jsonfd = open("compile_commands.json", "w+")

    def run_cmd(self, cmdline, logstdout=False):
        log = logging.getLogger(__name__)
        if self.DEBUG == True:
            sys.stdout.write(cmdline + "\n")
            sys.stdout.flush()
            return 0

        self.logfd.seek(0)
        log.info("{}".format(cmdline))
        self.cmdlog.write("{}\n".format(cmdline))
        try:
            ret = subprocess.run(cmdline, shell=True,\
                                stdout=self.logfd, stderr=self.logfd)
        except Exception as e:
            log.error(e, exc_info=True)

        retc = ret.returncode
        if (retc != 0) or (logstdout is True):
            self.logfd.flush()
            self.logfd.seek(0)
            for line in self.logfd:
                log.info(line.strip())
            self.logfd.close()
            ret.check_returncode()

        return retc

    def create_build_dir(self, dirpath):
        log = logging.getLogger(__name__)
        if dirpath == "":
            return

        if self.DEBUG == True:
            sys.stdout.write("Creating directory: " + dirpath)
            return
        log.info("Creating directory: " + dirpath)
        os.makedirs("build/" + dirpath, exist_ok=True)

    # Start with the clean()
    def clean_prev_build(self):
        if self.skip_clean is True:
            return
        log = logging.getLogger(__name__)
        log.info("Cleaning...")
        clean_cmd = 'rm -rf build/*'
        ret = subprocess.run(clean_cmd, shell=True)
        ret.check_returncode()

    def set_sources(self, src_list_file='SrcFilesList'):
        log = logging.getLogger(__name__)
        for srcname in open(src_list_file, 'r'):
            log.info("Adding: {}".format(srcname.strip()))
            self.SOURCES.append(srcname.strip())

    def compile(self):
        log = logging.getLogger(__name__)
        log.info("Compiling...")
        self.last_err = 0

        for inc_dir in self.INCDIRS:
            self.CFLAGS.append("-I" + inc_dir)

        cmd_line = self.CC_PREFIX + self.CC + " " + " ".join(self.CFLAGS)

        for src_file in self.SOURCES:
            filetype = src_file.split('.')[-1]
            if filetype == 'c':
                log.info("Compiling {}...".format(src_file))
                cmd_line = self.compile_cmd()
            elif filetype == 's':
                log.info("Assembling {}...".format(src_file))
                cmd_line = self.assemble_cmd()

            dirpath = "/".join(src_file.split("/")[:-1])
            self.create_build_dir(dirpath)
            obj_file = "build/"
            obj_file += "".join(src_file.split(".")[:-1]) + ".o"
            cmd = " ".join([cmd_line, "-o", obj_file, src_file])
            self.run_cmd(cmd)
            self.obj_files.append(obj_file)
            self.save_json(dirpath, src_file, obj_file, cmd_line)

    def save_json(self, dirname, srcfname, objname, cmdline):
        currwd = os.getcwd() + os.path.sep
        retdict = {}
        retdict['directory'] = currwd + dirname
        retdict['file'] = currwd + srcfname
        retdict['command'] = ' '.join([cmdline, '-o', objname, retdict['file']])
        self.jsonfd.write(json.dumps(retdict) + ',\n')

    def compile_cmd(self):
        cmd_line = self.CC_PREFIX + self.CC + " " + " ".join(self.CFLAGS)
        return cmd_line

    def assemble_cmd(self):
        cmd_line = self.CC_PREFIX + self.CC + " " + " ".join(self.CFLAGS)
        cmd_line += " -x assembler-with-cpp"
        return cmd_line

    def buildlib(self):
        cmd_line = self.CC_PREFIX + "ar -r "
        cmd_line += "build/lib" + self.ONAME + ".a "
        cmd_line += " ".join(self.obj_files) + " "
        self.run_cmd(cmd_line)

    def link(self):
        log = logging.getLogger(__name__)
        log.info("Linking...")
        cmd_line = self.CC_PREFIX + self.CC + " "
        cmd_line += " ".join(self.LDFLAGS) + " "
        cmd_line += "-T" + self.LDSCRIPT + " "
        for libpath in self.LIBPATHS:
            cmd_line += "-L" + libpath + " "

        cmd_line += " ".join(self.obj_files) + " "

        for libname in self.LIBS:
            cmd_line += "-l" + libname + " "

        cmd_line += " -o build/" + self.ONAME + ".elf"

        self.run_cmd(cmd_line)

    def objcopy(self):
        log = logging.getLogger(__name__)
        log.info("Generating hex and bin files...")
        for otype in ["binary", "ihex"]:
            cmd_line = self.CC_PREFIX + "objcopy"
            cmd_line += " -O " + otype
            cmd_line += " build/" + self.ONAME + ".elf"
            cmd_line += " build/" + self.ONAME + "." + otype
            self.run_cmd(cmd_line)

        log.info("Computing the output size: ")
        cmd_line = self.CC_PREFIX + "size"
        cmd_line += " build/" + self.ONAME + ".elf"
        self.run_cmd(cmd_line, True)


