import os
import sys
import subprocess


class Helper:
    def __init__(self):
        self.DEBUG = False
        self.CC_PREFIX = 'arm-none-eabi-'
        self.CC = 'gcc'
        self.logfd = None
        self.last_err = 0
        self.asmflags = []
        self.SOURCES = []
        self.INCDIRS = []
        self.obj_files = []
        self.OBJS = []

    def set_sources(self, src_list_file='SrcFilesList'):
        for srcname in open(src_list_file, 'r'):
            self.SOURCES.append(srcname)

    def compile(self):
        self.last_err = 0

        for inc_dir in self.INCDIRS:
            self.CFLAGS.append("-I" + inc_dir)

        cmd_line = self.CC_PREFIX + self.CC + " " + " ".join(self.CFLAGS)

        for src_file in self.SOURCES:
            if src_file.split('.')[-1] != 's':
                continue
            dirpath = "/".join(src_file.split("/")[:-1])
            self.create_build_dir(dirpath)
            obj_file = "build/"
            obj_file += "".join(src_file.split(".")[:-1]) + ".o"
            self.obj_files.append(obj_file)
            cmd = " ".join([cmd_line, "-o", obj_file, src_file])
            self.run_cmd(cmd)

