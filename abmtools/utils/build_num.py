import os
import logging


def get_build_num():
    try:
        fd = open("src/inc/BUILDNUM.h", "r+")
        for inln in fd:
            if '#define' in inln:
                break
        build_str = inln.split()[-1]
        build_num = int(build_str.replace('(', '').replace(')', '')) + 1
    except FileNotFoundError:
        build_num = 1
    return build_num


def update_build_num(build_num):
    log = logging.getLogger(__name__)
    log.info("Build: {}".format(build_num))
    fd = open("src/inc/BUILDNUM.h", "w+")
    fd.write("""
#ifndef BUILDNUM
#define BUILDNUM {}
#endif /* BUILDNUM */
    """.format(build_num))


def incr_build_num(args):
    if args.build_num is not None:
        build_num = int(args.build_num)
    else:
        build_num = get_build_num()
    buildnum = get_build_num()
    update_build_num(buildnum)
    return buildnum


def get_proj_name(args):
    if args.proj is not None:
        return args.proj
    return os.getcwd().split(os.path.sep)[-1]


# End of file

