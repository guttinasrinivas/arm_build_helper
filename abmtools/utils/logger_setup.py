import os
import sys
import logging
import collections
import logging.handlers
from common.utils import get_user_dir
from common.utils import generate_mod_fname


LG_MemHdlr = None


class ErrMemLogHandler:
    def __init__(self, *args, **kwargs):
        formatstr = kwargs.get('fmtstr', '%(asctime)s - %(levelname)s: %(message)s')
        self.fh = kwargs['lgfh']
        self.formatter = logging.Formatter(formatstr)
        self.buf = collections.deque(maxlen=160)
        self.level = logging.DEBUG

    def setLevel(self, lvl):
        self.level = lvl

    def setFormatter(self, fmtstr):
        self.formatter = logging.Formatter(fmtstr)

    def handle(self, msg):
        if msg.levelno < self.level:
            return
        txtmsg = self.formatter.format(msg)
        self.buf.append('{}\n'.format(txtmsg))
        if msg.levelno < logging.ERROR:
            return
        for logln in self.buf:
            self.fh.stream.write(logln)
        self.buf.clear()


def setup_logging(fhlevel=logging.INFO,
                  chlevel=logging.DEBUG,
                  mhlevel=logging.DEBUG,
                  logfname=None,
                  formatstr=None):
    global LG_MemHdlr

    # create logger
    logger = logging.getLogger()
    if chlevel < fhlevel:
        logger.setLevel(chlevel)
    else:
        logger.setLevel(fhlevel)

    if logfname is None:
        raise RunTimeError("Please setup a log file by name")
    
    ch = logging.StreamHandler()
    ch.setLevel(chlevel)
    fh = logging.FileHandler(logfname)
    fh.setLevel(fhlevel)
    sh = ErrMemLogHandler(lgfh=fh)
    sh.setLevel(mhlevel)

    # create formatter
    if formatstr is None:
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s: %(message)s')
    else:
        formatter = logging.Formatter(formatstr)

    # add formatter to ch
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    for r in logger.handlers:
        logger.removeHandler(r)
        
    # add ch to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    logger.addHandler(sh)
    LG_MemHdlr = sh
    logger.info("Logging to: {}".format(logfname))
    return logger


# logger = setup_logging()
