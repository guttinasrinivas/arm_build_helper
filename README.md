# ARM Bare Metal Build Helper

A very light weight and simple build tool written in Python aimed for building ARM Bare Metal applications for ARM Cortex M series micro-controllers (e.g. STM32 series, EFM32 series etc.)

Download a build, preferably the [latest](https://bitbucket.org/guttinasrinivas/arm_build_helper/downloads/abm-builder-1.0.19.tar.gz), from https://bitbucket.org/guttinasrinivas/arm_build_helper/downloads/ and install with something like `pip3 install abm-builder-x.x.x.tar.gz` command.

Uses arm-none-eabi-gcc tools for building. Other compiler collection could be used as well, though not tested. The ARM Cortex M toolchain `arm-none-eabi-*` could be downloaded from https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads site.

Written and tested on both Linux and Windows.